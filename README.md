# Que contient ce dépôt ?

Ce dépôt contient le fichier source d'un mémoire que j'ai rédigé pour l'université. Je ne comptais pas le partager au début, mais suite à plusieurs demandes de personnes souhaitant y avoir accès, j'ai partagé le code source.

Le fichier n'a pas vocation a être modifié, raison pour laquelle je n'autorise pas les forks.